package gramstad.fileinfo;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class FileInfo {


    public static void main(String[] args){

        if(args.length == 0){
            System.out.println("first arg must indicate input file path");
            return;
        }

        File inputFile = new File(args[0]);

        if(!inputFile.exists()){
            System.out.println("File does not exist at "+args[0]);
            return;
        }

        FileInfo fileInfo = new FileInfo();
        fileInfo.readFile(args[0]);

        System.out.println("Sum: "+fileInfo.getTotal());
        System.out.println("Average: "+fileInfo.getAverage());
        System.out.println("Median: "+fileInfo.getMedian());
        System.out.println("Percent of Numbers: "+fileInfo.numberPercentage());
        System.out.println("Distinct Strings with count:");
        fileInfo.printdistinctStrings();
    }

    List<BigDecimal> numbers;
    List<String> strings;

    public FileInfo(){
        this.numbers = new ArrayList<BigDecimal>();
        this.strings = new ArrayList<String>();
    }

    public void readFile(String filePath){
        FileReader inputFileReader = null;

        try {
            inputFileReader = new FileReader(filePath);
        } catch (FileNotFoundException e) {
            System.out.println("No file found at path: "+filePath);
        }

        BufferedReader br = new BufferedReader(inputFileReader);

        try{

            String line = br.readLine();
            while (line != null) {


                //determine if it is a string or integer

                try{
                    BigDecimal dec = new BigDecimal(line);
                    numbers.add(dec);

                }catch (NumberFormatException e){
                    //is string
                    strings.add(line);
                }

                line = br.readLine();
            }
        }catch (IOException e){
            System.out.println("Error while reading input file.");
        }
    }

    //sum
    private BigDecimal sum(){

        BigDecimal sum = new BigDecimal("0");

        for (BigDecimal number: numbers){
            sum = sum.add(number);
        }

        return sum;
    }

    public double getTotal(){
        return sum().setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    //average
    private BigDecimal average(){

        BigDecimal sum = sum();

        BigDecimal average = sum.divide(new BigDecimal(numbers.size()));

        return average;
    }

    public double getAverage(){
        return average().setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    //median
    private BigDecimal median(){
        //sort numbers
        BigDecimal[] sortedArraySize = new BigDecimal[numbers.size()];

        BigDecimal[] sortedArray = (BigDecimal[]) numbers.toArray(sortedArraySize);

        Arrays.sort(sortedArray);

        BigDecimal median;

        if (sortedArray.length % 2 == 0)
            median = (sortedArray[sortedArray.length/2].add(sortedArray[sortedArray.length/2 - 1])).divide(BigDecimal.valueOf(2));
        else
            median = sortedArray[sortedArray.length/2];

        return  median;
    }

    public double getMedian(){
        return median().setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    public double numberPercentage(){

        BigDecimal numbersSize = new BigDecimal(numbers.size());
        BigDecimal stringsSize = new BigDecimal(strings.size());

        BigDecimal numberPercent = numbersSize.divide(stringsSize.add(numbersSize), 4, RoundingMode.HALF_UP);

        return numberPercent.multiply(BigDecimal.valueOf(100)).doubleValue();
    }

    public int getCountOfNumbers(){
        return numbers.size();
    }

    public boolean contains(String src){
        return strings.contains(src);
    }

    public void printdistinctStrings(){

        List<String> inputStrings = strings;
        inputStrings.sort(new SortIgnoreCase());

        Iterator<String> iterator = inputStrings.iterator();

        String previousEntry = null;
        int stringCount = 1;

        while(iterator.hasNext()){

            String entry = iterator.next();

            if(entry.equals(previousEntry)){
                stringCount++;
            }else if(previousEntry != null) {
                System.out.println("    "+previousEntry+":"+stringCount);
                stringCount = 1;
            }

            previousEntry = entry;
        }

        System.out.println("    "+previousEntry+":"+stringCount);

    }

    class SortIgnoreCase implements Comparator<Object> {
        public int compare(Object o1, Object o2) {
            String s1 = (String) o1;
            String s2 = (String) o2;
            return s2.toLowerCase().compareTo(s1.toLowerCase());
        }
    }

}

